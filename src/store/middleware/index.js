import { applyMiddleware } from "redux";
import { loginMiddleware } from "./loginMiddleware";
import { sessionMiddleware } from "./sessionMiddleware";

import {profileMiddleware} from "./profileMiddleware";

export default applyMiddleware(
    loginMiddleware,
    sessionMiddleware,
    profileMiddleware
)