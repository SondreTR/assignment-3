import { LoginAPI } from "../../components/Login/LoginAPI"
import { ACTIONS_LOGIN_ATTEPTING, ACTIONS_LOGIN_SUCCESS, loginErrorAction, loginSuccessAction } from "../actions/loginActions"
import { sessionSetAction } from "../actions/sessionActions"

export const loginMiddleware = ({ dispatch }) => next => action => {
    
    next(action)

    if (action.type === ACTIONS_LOGIN_ATTEPTING){
        //make an http request to try and login
        LoginAPI.login(action.payload)
        .then(profile => {
            dispatch(loginSuccessAction(profile) )
        })
        .catch(error => {
            //error
            dispatch(loginErrorAction(error.message))
        })
    }

    if (action.type === ACTIONS_LOGIN_SUCCESS){
        dispatch(sessionSetAction(action.payload))
    }
    
}