import {
    PROFILE_TRANSLATION_ATTEMPTING,
    PROFILE_TRANSLATION_SET,
    translateSetAction,
    // translateAttemptAction, 
    translateErrorAction
} from '../actions/profileActions';
import {ProfileAPI} from "../../components/Profile/ProfileAPI";
import { sessionSetAction } from '../actions/sessionActions';

export const profileMiddleware = ({dispatch}) => next => action => {
    
    next(action)

    if (action.type === PROFILE_TRANSLATION_ATTEMPTING) {
        console.log(action.payload)
        ProfileAPI.addTranslation(action.payload)
            .then((translations) => dispatch(translateSetAction(translations)))
            .catch(error => (
                dispatch(translateErrorAction(error.message))
            ))
    }

    if (action.type === PROFILE_TRANSLATION_SET) {
        dispatch(sessionSetAction(action.payload))
    }
}
