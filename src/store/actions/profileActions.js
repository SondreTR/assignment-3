export const PROFILE_TRANSLATION_ATTEMPTING = '[translating] ATTEMPT'
export const PROFILE_TRANSLATION_SET = '[translating] SET'
export const PROFILE_TRANSLATION_ERROR = '[translating] ERROR'

export const translateAttemptAction = profile => ({
    type: PROFILE_TRANSLATION_ATTEMPTING,
    payload: profile
})

export const translateSetAction = profile => ({
    type: PROFILE_TRANSLATION_SET,
    payload: profile
})

export const translateErrorAction = error => ({
    type: PROFILE_TRANSLATION_ERROR,
    payload: error
})
