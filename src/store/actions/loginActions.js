export const ACTIONS_LOGIN_ATTEPTING = '[login] ATTEMPT'
export const ACTIONS_LOGIN_SUCCESS = '[login] SUCCESS'
export const ACTIONS_LOGIN_ERROR = '[login] ERROR'


export const loginAttemptAction = credentials => ({
    type: ACTIONS_LOGIN_ATTEPTING,
    payload: credentials
})

export const loginSuccessAction = profile => ({
    type: ACTIONS_LOGIN_SUCCESS,
    payload: profile
})

export const loginErrorAction = error => ({
    type: ACTIONS_LOGIN_ERROR,
    payload: error
})

