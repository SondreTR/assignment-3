import { ACTIONS_LOGIN_ATTEPTING, ACTIONS_LOGIN_ERROR,
    ACTIONS_LOGIN_SUCCESS } from "../actions/loginActions"

const initalState = {
   loginAttempting: false,
   logoutAttempting: false,
   loginError: ''
}

export const loginReducer = (state = initalState, action) => {
   switch (action.type){
       case ACTIONS_LOGIN_ATTEPTING:
           return {
               ...state,
               loginAttempting : true,
               loginError: ''
           }
       case ACTIONS_LOGIN_SUCCESS:
           return {
               ...initalState
           }
       case ACTIONS_LOGIN_ERROR:
           return {
               ...state,
               loginAttempting: false,
               loginError: action.payload
           }
       default:
           return state
   }
}