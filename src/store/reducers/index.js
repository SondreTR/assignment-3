import { combineReducers } from "redux";
import { loginReducer } from "./loginReducer";
import { sessionReducer } from "./sessionReducer";
import { profileReducer } from "./profileReducer";


const appReducer = combineReducers({
    loginReducer,
    sessionReducer,
    profileReducer
})

export default appReducer