import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Login from './components/Login/Login'
import NotFound from './components/NotFound/NotFound';
import Translator from './components/Translator/Translator';
import Profile from './components/Profile/Profile';

function App() {
  return (
    <BrowserRouter>
      
      <div className="App">
        <h1>React Translator</h1>
      </div>

      <Switch>
        <Route path='/' exact component={ Login } /> 
        <Route path="/profiles/:id" component={ Profile }/>
        <Route path="/translate" component={ Translator } />
        <Route path='*' component={ NotFound } />
      </Switch>

    </BrowserRouter>
  );
}

export default App;
