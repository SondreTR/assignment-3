export const ProfileAPI = {

    addTranslation(profile) {
        return fetch('http://localhost:5000/profiles/' + profile.id, {
            method: 'PATCH', 
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({translations: profile.translations})
        })
        .then(async (response) => {
            if (!response.ok) { throw new Error("Could not compute") }
            return response.json()
        })
    }
}
