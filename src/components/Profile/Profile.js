import AppContainer from "../../hoc/AppContainer";
import {useSelector, useDispatch} from "react-redux";
import {useHistory} from "react-router-dom";
import {translateAttemptAction} from "../../store/actions/profileActions";




const Profile = () => {
    const dispatch = useDispatch()
    const {id, name, translations} = useSelector(state => state.sessionReducer)
    let history = useHistory();

    const goToTranslator = () => {
        history.push("/translate")
    }

    const resertTranslated = event => {
        console.log("test")    
        dispatch(translateAttemptAction({
            id,
            translations: []
        }))
    }

    return (
        <>
            <main>
                <div>
                    <div className="d-flex flex-column">
                        <h1>Welcome to your profile {name}</h1>
                    </div>
                </div>
            </main>

            <AppContainer>
                {translations &&
                <section>
                    <button onClick={goToTranslator}>Go Translate!</button>
                    <ul>
                        {translations.map(function(item, i){ return <li>{translations[i ]}</li>})}
                    </ul>
                    <button onClick={resertTranslated}>Delete</button>

                </section>}

            </AppContainer>

        </>
    )

}

export default Profile
