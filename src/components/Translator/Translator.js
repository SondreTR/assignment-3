import AppContainer from "../../hoc/AppContainer";
import {useDispatch, useSelector} from "react-redux";
import {useState} from "react";
import {translateAttemptAction} from "../../store/actions/profileActions";
import {Redirect} from "react-router-dom";
import {useHistory} from "react-router-dom";


const Translator = () => {

    const {id, name, translations, loggedIn} = useSelector(state => state.sessionReducer)
    const dispatch = useDispatch()
    const [textToTranslate, setTextToTranslate] = useState('')
    const [signLanguageText, setSignLanguageText] = useState('')
    let history = useHistory();

    const goToProfile = () => {
        history.push("/profiles/" + name)
    }

    const onInputChange = event => {
        setTextToTranslate(event.target.value.trim())
    }

    const displayTranslation = () => {
        let text = textToTranslate.replace(/[^a-zA-Z]/, '')
        text = text.toLowerCase()
        setSignLanguageText(text)
    }

    const onFormSubmit = event => {

        event.preventDefault()
        displayTranslation()    
        dispatch(translateAttemptAction({
            id,
            translations: [...translations.slice(-9), textToTranslate]
        }))
    }

    return (
        <> {!loggedIn && <Redirect to="/"/>}
            {loggedIn &&
            <>
                <button onClick={goToProfile}>Go to profile</button>
                <main onSubmit={onFormSubmit}>
                    <div className="input-group-text rounded-pill bg-light container">
                        <span className="material-icons ">keyboard</span>

                        <input id="translation" type="text"
                               placeholder="What do you want to translate?"
                               className="form-control rounded-pill border-0"
                               onChange={onInputChange}/>
                        <span className="material-icons" type="submit" onClick={onFormSubmit}>arrow_forward</span>
                    </div>
                </main>
                <AppContainer>
                        {signLanguageText.split('').map((char, index) => (
                            <img src={`./images/${char}.png`} alt={"Sign language symbol"} key={index}/>))
                        }
                </AppContainer>
            </>}
        </>
    )
}

export default Translator
