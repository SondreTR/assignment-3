import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { loginAttemptAction } from "../../store/actions/loginActions"

const Login = () => {

    const dispatch = useDispatch()
    const { loginAttempting } = useSelector(state => state.loginReducer)
    const { loggedIn } = useSelector(state => state.sessionReducer)


    const [ credentials, setCredentials ] = useState({
        name: '',
        translations: []
    })

    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value.trim()
        })
    }

    const onRegisterSubmit = event => {
        event.preventDefault() //stop the page reload
        
        if(credentials.name === "") {
            alert("Name must contain chars")
            return
        }
        
        dispatch(loginAttemptAction(credentials))
    }

    return (

        <>
            { loggedIn && <Redirect to="/translate" /> }
            
            { !loggedIn && 
                <AppContainer>
                    <form className="mt-3 mb-3" onSubmit={ onRegisterSubmit }>
                        <div className="input-group-text rounded-pill bg-light mb-3">

                            <input id="name" type="text"
                                placeholder="Enter username"
                                className="form-control rounded-pill border-0"
                                onChange={onInputChange}/>

                            <button className="rounded-pill" type="submit">Submit</button>

                        </div>

                    </form>
                    
                    { loginAttempting && <p> Trying to login...</p>}
                </AppContainer>
            }

        </>
    )
}

export default Login