export const LoginAPI = {
  async login(credentials) {
    const existingUser = await checkUser(credentials.name);

    if (existingUser.length) {
      return new Promise((resolve) => resolve(existingUser.pop()));
    }

    return fetch("http://localhost:5000/profiles", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(credentials),
      // body: JSON.stringify({ name: credentials.name, translations: []})
    }).then(async (response) => {
      if (!response.ok) {
        throw new Error("could not register");
      }
      return response.json();
    });
  },
};

async function checkUser(username) {
  return fetch("http://localhost:5000/profiles?name=" + username).then(
    handleFirstResponse
  );
}

function handleFirstResponse(response) {
  return response.json();
}
